package main.java;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double num1, num2;
        System.out.println("Введіть перше число:");
        num1 = scanner.nextDouble();
        System.out.println("Введіть друге число:");
        num2 = scanner.nextDouble();
        System.out.println("Виберіть операцію (+, -, *, /, t - площина трикутника):");
        char operation = scanner.next().charAt(0);
        double result;
        switch (operation) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                if (num2 == 0) {
                    System.out.println("Ділення на нуль неможливе");
                    return;
                }
                result = num1 / num2;
                break;
            case 't':
                result = calculateTriangleArea(num1, num2);
                break;
            default:
                System.out.println("Невірна операція");
                return;
        }
        System.out.println("Результат: " + result);
    }
    public static double calculateTriangleArea(double side1, double side2) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введіть величину кута між сторонами (в градусах):");
        double angle = scanner.nextDouble();

        double angleInRadians = Math.toRadians(angle);

        return 0.5 * side1 * side2 * Math.sin(angleInRadians);
    }
}